package com.example.shoppingapp1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.squareup.okhttp.Callback
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import com.squareup.okhttp.Response
import org.json.JSONObject
import java.io.IOException
import java.util.concurrent.CountDownLatch

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        Thread{
            getProductData()
            startActivity(Intent(this@SplashScreenActivity, HomeActivity::class.java))
            finish()
        }.start()
    }

    private fun getProductData() {

        val countDownLatch = CountDownLatch(ProductInfo.PRODUCT_LIST_URLS.size)

        for( category in 0 until ProductInfo.PRODUCT_LIST_URLS.size){

            OkHttpClient().newCall(Request.Builder().url(ProductInfo.PRODUCT_LIST_URLS[category]).build()).enqueue(object : Callback {
                override fun onResponse(response: Response?) {
                    val jsonArray = JSONObject(response!!.body().string()).getJSONArray("Products")

                    for (i in 0 until jsonArray.length()) {
                        val item = jsonArray.getJSONObject(i)
                        ProductInfo.productLists[category].add(item)

                        if(item.has("category"))
                            //Some products fall into multiple categories (ex "Jewellery,Accessories") so add each individually.
                            for(cat in item.getString("category").split(","))
                                ProductInfo.productCategories[category].add(cat)
                        else
                            ProductInfo.productCategories[category].add("Other")
                    }
                    countDownLatch.countDown()
                }
                override fun onFailure(request: Request?, e: IOException?) = finish()
            })
        }
        //wait for all product lists to be processed before continuing.
        countDownLatch.await()
    }
}