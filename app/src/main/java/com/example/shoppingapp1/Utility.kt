package com.example.shoppingapp1

import android.content.Context
import android.util.DisplayMetrics

class Utility {
    companion object{
        /**
         * Converts integer values to dot pixels on the given device.
         */
        fun Int.toPx(context: Context) = this * context.resources.displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT
    }
}