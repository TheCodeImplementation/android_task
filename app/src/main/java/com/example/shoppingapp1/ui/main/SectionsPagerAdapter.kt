package com.example.shoppingapp1.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.shoppingapp1.R

private val TAB_TITLES = arrayOf(
    R.string.tab_text_1,
    R.string.tab_text_2,
    R.string.tab_text_3,
    R.string.tab_text_4,
    R.string.tab_text_5,
)

class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        when(position){
            in 0..3 -> return ProductCategoriesFragment(position)
            else    -> return MannequinFragment()
        }
    }

    override fun getPageTitle(position: Int) = context.resources.getString(TAB_TITLES[position])
    override fun getCount() = TAB_TITLES.size
}