package com.example.shoppingapp1.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.example.shoppingapp1.myviews.MySliderLayout
import com.example.shoppingapp1.FinderActivity
import com.example.shoppingapp1.ProductInfo
import com.example.shoppingapp1.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.mannequin_fragment.*
import kotlinx.android.synthetic.main.mannequin_full.*
import kotlinx.android.synthetic.main.mannequin_section.view.*
import java.util.ArrayList

class MannequinFragment : Fragment() {

    private val FINDER_REQ_CODE = 1234

    lateinit var mannequinSections : List<View>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.mannequin_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mannequinSections = listOf(mqHead, mqTorso, mqLegs, mqFeet)
        initializeButtons()
    }

    /**
     * The mannequin starts "life" as multiple buttons with a figure's silhouette as the background.
     * This method creates those buttons. When clicked, the "finder" activity will be launched and
     * that activity will return a list of products indices
     */
    private fun initializeButtons(){

        val imageList = listOf(
            R.drawable.woman_head_plus,
            R.drawable.woman_torso_plus,
            R.drawable.woman_legs_plus,
            R.drawable.woman_feet_plus
        )

        mannequinSections.forEachIndexed { mqIndex , mqSection ->
            mqSection.btnMannequin.apply{
                background = resources.getDrawable(imageList[mqIndex])
                setOnClickListener { btn ->
                    val indexOfBtn = (mqSection as LinearLayout).indexOfChild(btn)
                    val intent = Intent(activity!!, FinderActivity::class.java)
                    intent.putExtra("mqIndex", mqIndex)
                    intent.putExtra("indexOfBtn", indexOfBtn)
                    startActivityForResult(intent, FINDER_REQ_CODE)
                }
            }
        }
    }

    /**
     * Creates a MySliderLayout populated with images of the chosen products. When swiped,
     * the budget will be updated as will section's product name and cost.
     */
    fun addViewPager(section: LinearLayout, indexOfBtn: Int,  list: ArrayList<Int>?) {
        val products = ProductInfo.productLists[ProductInfo.WOMEN]
        val images = mutableListOf<View>()
        list?.forEach{ index ->
            val jsonArray = products[index].getJSONArray("allImages")
            images.add(
                ImageView(context!!).apply{
                    Picasso.with(context!!)
                        .load(
                            jsonArray[jsonArray.length()-1].toString())
                        .into( this )
                }
            )
        }
        val slider = MySliderLayout(
            activity!!,
            90,
            mutableListOf( //list of actions to occur when swiped
                { position: Int, previousPosition: Int? ->
                    section.itemName.text = products[list!![position]].getString("name")
                    section.itemCost.text = products[list!![position]].getString("cost")
                },
                { position, previousPosition ->
                    var previous : Int?  = 0
                    if(previousPosition != null)
                        previous = products[list!![previousPosition]].getString("cost").toInt()
                    val current = products[list!![position]].getString("cost").toInt()
                    var cost = txtBudget.text.toString().toInt()

                    if(previousPosition != null)
                        cost += products[list!![previousPosition]].getString("cost").toInt()
                    cost -= products[list!![position]].getString("cost").toInt()
                    txtBudget.text = "$cost"
                }
            )
        ).apply {
            addAllChildren(images) //add images to MySliderLayout
        }
        //replace the button with the newly created slider
        section.removeViewAt(indexOfBtn)
        section.addView(slider, indexOfBtn)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == FINDER_REQ_CODE){
            val mqIndex = data!!.getIntExtra("mqIndex", -1)
            val indexOfBtn = data.getIntExtra("indexOfBtn", -1)
            val productIndices = data.getIntegerArrayListExtra("productIndices")
            addViewPager(
                mannequinSections[mqIndex] as LinearLayout,
                indexOfBtn,
                productIndices
            )
        }
    }
}