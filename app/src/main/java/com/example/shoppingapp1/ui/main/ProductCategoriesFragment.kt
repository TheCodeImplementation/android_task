package com.example.shoppingapp1.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.shoppingapp1.ProductInfo
import com.example.shoppingapp1.ProductsActivity
import com.example.shoppingapp1.Utility.Companion.toPx
import com.squareup.picasso.Picasso

class ProductCategoriesFragment(val position : Int) : Fragment() {

    lateinit var lv : ListView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        lv = ListView(context)
        return lv
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //setup the category's header/hero image
        val headerImage = ImageView(context)
        headerImage.layoutParams = AbsListView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 200.toPx(context!!))
        val padding = 10.toPx(context!!)
        headerImage.setPadding(padding, padding, padding, padding)
        Picasso.with(context!!).load(ProductInfo.heroImages[position]).into(headerImage)
        lv.addHeaderView(headerImage)

        lv.adapter = ArrayAdapter(context!!,
            android.R.layout.simple_expandable_list_item_1,
            ProductInfo.productCategories[position].toList()
        )

        headerImage.setOnClickListener { startActivity(createIntent(null)) }
        lv.setOnItemClickListener { _, _view,_,_ -> startActivity(createIntent((_view as TextView).text.toString())) }
    }

    fun createIntent(subcategory : String? ) : Intent {
        val intent = Intent(this.context, ProductsActivity::class.java)
        intent.putExtra("category", position)
        intent.putExtra("sub-category", subcategory)
        return intent
    }
}