package com.example.shoppingapp1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.FrameLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ProductsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val frameLayout = FrameLayout(this)
        val recyclerView = RecyclerView(this)

        recyclerView.layoutManager = GridLayoutManager(this, 2)

        val category = intent.getIntExtra("category", -1)
        val subCategory = intent.getStringExtra("sub-category")

        //populate array with products matching the given sub-category (ex. "Jeans")
        val arr = if( subCategory == null ) {
            ProductInfo.productLists[category]
        }else{
            ProductInfo.productLists[category].filter{
                if(it.has("category"))
                    it.getString("category").contains(subCategory)
                else
                    subCategory == "Other"
            }
        }

        recyclerView.adapter = RecyclerAdapter(arr, this)
        frameLayout.addView(recyclerView)
        addContentView(frameLayout, FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
    }
}