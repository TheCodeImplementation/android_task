package com.example.shoppingapp1

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.shoppingapp1.Utility.Companion.toPx
import kotlinx.android.synthetic.main.activity_finder.*

/**
 * At the moment, this class is just a place holder for what will become "Finder" - the Tinder-like
 * clothes selection feature.
 */

class FinderActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_finder)

        val mqIndex = intent.getIntExtra("mqIndex", -1)
        var header = ""
        val productIndices = ArrayList<Int>()

        when(mqIndex){
            0 -> {
                header = "Head"
                productIndices.addAll(findProductIndicesByName("hat"))
//                productIndices.addAll(findProductIndicesByCategory("Accessories"))
            }
            1 -> {
                header = "Torso"
                productIndices.addAll(findProductIndicesByCategory("Tops"))
            }
            2 -> {
                header = "Legs"
                productIndices.addAll(findProductIndicesByCategory("Trousers"))
                productIndices.addAll(findProductIndicesByCategory("Skirts"))
            }
            3 -> {
                header = "Feet"
                productIndices.addAll(findProductIndicesByCategory("Shoes And Boots"))
            }
            else -> {
                header = "error"
                productIndices.addAll(findProductIndicesByCategory("hat"))
            }

        }

        txtFinderHeader.text = header

        intent.putIntegerArrayListExtra("productIndices", productIndices)

        btnBack.setOnClickListener {
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    private fun findProductIndicesByCategory(category : String) : List<Int> {

        val list = mutableListOf<Int>()

        var i = 0
        for(product in ProductInfo.productLists[ProductInfo.WOMEN]){
            if(product.getString("category") == category){
                list.add(i)
            }
            if(list.size > 10)
                break
            i++
        }
        return list
    }

    private fun findProductIndicesByName(substring : String) : List<Int> {

        val list = mutableListOf<Int>()

        var i = 0
        for(product in ProductInfo.productLists[ProductInfo.WOMEN]){
            if(product.getString("name").toLowerCase().contains(substring)){
                list.add(i)
            }
            if(list.size > 10)
                break
            i++
        }
        return list
    }
}