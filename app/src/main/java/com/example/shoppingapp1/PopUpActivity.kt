package com.example.androidscratchpad.ui.main

import com.example.shoppingapp1.myviews.MySliderLayout
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatDialogFragment
import com.example.shoppingapp1.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.image_popup.view.*
import org.json.JSONObject

class PopUpActivity(val index : Int, val productList : List<JSONObject>) : AppCompatDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)

        val layout = activity!!.layoutInflater.inflate(R.layout.image_popup, null)

        val slider = MySliderLayout(context!!, 400)
        val images = mutableListOf<View>()
        val jsonArray = productList[index].getJSONArray("allImages")
        for(i in 0 until jsonArray.length()) {
            slider.addChild(
                ImageView(context).apply{
                    Picasso.with(context!!)
                        .load(
                            jsonArray[i].toString())
                        .into( this )
                }
            )
        }

        layout.llItem.addView(slider, 0)

        builder.setView(layout)
        return builder.create()
    }
}