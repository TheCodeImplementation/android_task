package com.example.shoppingapp1

import org.json.JSONObject
import java.util.*

object ProductInfo {

    val WOMEN = 0
    val MEN   = 1
    val GIRLS = 2
    val BOYS  = 3

    val PRODUCT_LIST_URLS = arrayOf(
        "https://static-r2.ristack-3.nn4maws.net/v1/plp/en_gb/2506/products.json",
        "https://static-r2.ristack-3.nn4maws.net/v1/plp/en_gb/3600/products.json",
        "https://static-r2.ristack-3.nn4maws.net/v1/plp/en_gb/2145/products.json",
        "https://static-r2.ristack-3.nn4maws.net/v1/plp/en_gb/2048/products.json"
    )

    val productLists = List<MutableList<JSONObject>>(4, { mutableListOf()})

    val productCategories = List<SortedSet<String>>(4, { sortedSetOf() } )

    val heroImages = arrayOf(
        //1200*344 images
        "https://images.riverisland.com/is/image/RiverIsland/c20200309-wwlp-BANNER_UPDATE_DNT?hg&\$desktop\$",
        "https://images.riverisland.com/is/image/RiverIsland/c20200209-mwlp-BANNER_DNT?mn&\$desktop\$",
        "https://images.riverisland.com/is/image/RiverIsland/c20200309-gwlp-BANNER_DNT?&\$desktop\$",
        "https://images.riverisland.com/is/image/RiverIsland/c20200309-bwlp-BANNER_UPDATE_DNT?ab&\$desktop\$"
    )
}