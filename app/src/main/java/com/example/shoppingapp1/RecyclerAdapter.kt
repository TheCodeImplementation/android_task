package com.example.shoppingapp1

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.androidscratchpad.ui.main.PopUpActivity
import com.squareup.picasso.Picasso
import org.json.JSONObject

class RecyclerAdapter(val productsList : List<JSONObject>, val context : Context) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

        val itemPicture = itemView.findViewById<ImageView>(R.id.ivItemPicture)
        val itemName = itemView.findViewById<TextView>(R.id.txtItemName)
        val itemCost = itemView.findViewById<TextView>(R.id.txtItemCost)

        init {
            itemView.setOnClickListener{
                val p = PopUpActivity(adapterPosition, productsList)
                p.show( (context as AppCompatActivity).supportFragmentManager, "tag")
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.apply {
            Picasso
                .with(itemPicture.context)
                .load(
                    if(productsList[position].getString("altImage") != "")
                        productsList[position].getString("altImage")
                    else
                        productsList[position].getJSONArray("allImages")[0].toString()
                )
                .into(itemPicture)
            itemName.text = productsList[position].getString("name")
            itemCost.text = "£${productsList[position].getInt("cost")}" //todo: currency symbol varies with settings/region
        }
    }

    override fun getItemCount() = productsList.size
}