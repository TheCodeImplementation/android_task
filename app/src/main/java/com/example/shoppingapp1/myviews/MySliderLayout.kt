package com.example.shoppingapp1.myviews

import android.content.Context
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.shoppingapp1.Utility.Companion.toPx
import kotlin.math.roundToInt

class MySliderLayout(
    context : Context,
    var size : Int = 200,
    private val onSwipeActions : MutableList<((position: Int, previousPosition: Int?) -> Unit)?> = mutableListOf()
) : ViewPager(context) {

    var children : MutableList<View> = mutableListOf(  )
        set(value){
            field = value
            adapter?.notifyDataSetChanged()
        }
    private var previousPosition : Int? = null

    init {
        size                    = size.toPx(context)
        val scalerMin           = .7f
        val sidePadding    = (size * .1).roundToInt()
        val ammendedWidth  = size + sidePadding * 2
        val pageCentreSpot= (sidePadding*2)/(size+ammendedWidth).toFloat()
        adapter                 = MyAdapter()
        layoutParams            = LayoutParams(ammendedWidth, size)
        pageMargin              = -(((size * (1 - scalerMin)) / 2) + 7).roundToInt()
        clipToPadding           = false
        setPadding(sidePadding, 0, sidePadding, 0)

        //animation: page shrinks as it moves away from the centre position
        setPageTransformer(false, object : PageTransformer {
            override fun transformPage(page: View, position: Float) {
                val scale = Math.max(scalerMin, 1 - Math.abs(position - pageCentreSpot))
                page.apply{
                    scaleX = scale
                    scaleY = scale
                    alpha = scale
                }
            }
        })

        addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageSelected(position: Int) {
                onPageChange(position)
            }
            override fun onPageScrolled(position: Int,positionOffset: Float,positionOffsetPixels: Int) {}
            override fun onPageScrollStateChanged(state: Int) {}
        })

        onPageChange(0)
    }

    fun onPageChange(currentPage : Int){
        onSwipeActions.forEach{
            it?.invoke(currentPage, previousPosition)
        }
        previousPosition = currentPage
    }

    fun addChild(view : View){
        children.add(view)
        adapter?.notifyDataSetChanged()
    }

    fun addAllChildren(list : MutableList<View>){
        children.addAll(list)
        adapter?.notifyDataSetChanged()
    }

    fun addSwipeAction(action : ((position:Int, previousPosition: Int?) -> Unit)?){
        onSwipeActions.add(action)
    }

    private inner class MyAdapter : PagerAdapter() {

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val v = children[position]
            container.addView(v)
            return v
        }
        override fun isViewFromObject(view: View, `object`: Any) = view === `object`
        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) = container.removeView(`object` as View?)
        override fun getCount() = children.size
    }
}